%{
#include <stdio.h>
int yylex(void);
void yyerror(char *);
int sym[26];
extern FILE *yyin;
%}
%token INTEGER VARIABLE IF ELSE
%left '+' '-'
%left '*' '/'

%%
program:
program statement '\n'
|
;
statement:
expr';'{ printf("%d\n",$1); }
| VARIABLE '=' expr';'{ sym[$1] = $3; $$=$3; }
|IF '('boolexpr')' statement ELSE statement {
    if($3)
    {$$=$5;
    printf("%d\n",$$);

    }
    else 
    {$$=$7;
    printf("%d\n",$$);}
}
|IF '('boolexpr')' statement {if($3)
    {$$=$5;

    }}
;

boolexpr:
boolexpr'&''&'boolexpr {$$=$1&&$2;}
|boolexpr'|''|'boolexpr {$$=$1||$2;}
|VARIABLE'>'VARIABLE {$$=($1>$3);}
|VARIABLE'<'VARIABLE {$$=($1<$3);}
|VARIABLE'=''='VARIABLE {$$=($1==$3);}
|VARIABLE'>''='VARIABLE {$$=($1>=$3);}
|VARIABLE'<''='VARIABLE {$$=($1<=$3);}
|VARIABLE'>'INTEGER {$$=($1>$3);}
|VARIABLE'<'INTEGER {$$=($1<$3);}
|VARIABLE'=''='INTEGER {$$=($1==$3);}
|VARIABLE'>''='INTEGER{$$=($1>=$3);}
|VARIABLE'<''='INTEGER {$$=($1<=$3);}
;

expr:
INTEGER
| VARIABLE { $$ = sym[$1]; }
| expr '+' expr { $$ = $1 + $3; }
| expr '-' expr { $$ = $1 - $3; }
| expr '*' expr { $$ = $1 * $3; }
| expr '/' expr { $$ = $1 / $3; }
| '(' expr ')' { $$ = $2; }
;
%%
void yyerror(char *s) {
fprintf(stderr, "%s\n", s);

}
int main(void) {
yyin=fopen("test.txt","r");
yyparse();
return 0;
}