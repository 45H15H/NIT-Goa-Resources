%{
    #include<stdio.h>
    int yylex(void);
    void yyerror(char *);
    int sym[26];
    int count = 1;
    void printMultiCode(int,int);
    void printPlusCode(int,int);
    void printYCode(int);
%}


%token ID
%token NUM
%token FLOAT

%%

start:  ID '='  expr {printYCode($3);}


expr: expr '+'  term { printPlusCode($1,$3); $$ = 84;} 
    | term {$$ = $1; }
    ;

term:   term  '*' value {printMultiCode($1,$3); $$ = 84;} 
    | value  {$$ = $1;}
    ;

value: NUM { $$ = 48+$1;}
    

%%

void yyerror(char *s){
    printf("%s Invalid Expression\n", s);
}
void printPlusCode(int op1,int op2){
    if(op1>59){
        printf("T%d = %c%d + %c\n",count+1,op1,count,op2);
        count++;
    }
    else if(op2>59){
        printf("T%d = %c + %c%d\n",count+1,op1,op2,count);
        count++;
    }
    else{
        printf("T%d = %c + %c\n",count,op1,op2);
    }
}
void printMultiCode(int op1,int op2){
    if(op1>59 && op2>59){
        printf("T%d = %c%d * %c\n",count+1,op1,count,op2);
        count++;
    }
    if(op1>59){
        printf("T%d = %c%d * %c\n",count+1,op1,count,op2);
        count++;
    }
    else if(op2>59){
        printf("T%d = %c * %c%d\n",count+1,op1,op2,count);
        count++;
    }
    else{
        printf("T%d = %c * %c\n",count,op1,op2);
    }
}

void printYCode(int op){
    if(op>59){
        printf("Y = T%d\n",count);
    }
    else {
        printf("Y = %c\n",op);
    }
}

int main(void){
    printf("Input: ");
    yyparse();
    return 0;
}