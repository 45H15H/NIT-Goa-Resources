%{
#include"y.tab.h"
#include<stdio.h>
int yylex(void);
void yyerror(char *s);
char addtotable(char,char,char);
int index1=0;
char temp = 'A'-1;


struct expr{
char operand1;
char operand2;
char operator;
char result;
};
%}

%union{
char symbol;
}

%left '+' '-'
%left '/' '*'

%token <symbol> LETTER NUMBER
%type <symbol> exp
%%

statement: LETTER '=' exp ';' {addtotable((char)$1,(char)$3,'=');};
exp: exp '+' exp {$$ = addtotable((char)$1,(char)$3,'+');}
    |exp '-' exp {$$ = addtotable((char)$1,(char)$3,'-');}
    |exp '/' exp {$$ = addtotable((char)$1,(char)$3,'/');}
    |exp '*' exp {$$ = addtotable((char)$1,(char)$3,'*');}
    |'(' exp ')' {$$= (char)$2;}
    |NUMBER {$$ = (char)$1;}
    |LETTER {(char)$1;};

%%

struct expr arr[20]; 

char addtotable(char a, char b, char o){
    temp++;
    arr[index1].operand1 = a;
    arr[index1].operand2 = b;
    arr[index1].operator = o;
    arr[index1].result=temp;
    index1++;
    return temp;
}

// we have data in the array
void threeAdd(){

    int i=0;
    char temp='A';
    while(i<index1){
        printf("T%d := ",arr[i].result-'A'+1);

        if(arr[i].operand1<=59) printf("%c ",arr[i].operand1);
        else printf("T%d ",arr[i].operand1-'A'+1);

        printf("%c ",arr[i].operator);

        if(arr[i].operand2<=59) printf("%c ",arr[i].operand2);
        else printf("T%d ",arr[i].operand2-'A'+1);

        i++;
        temp++;
        printf("\n");
    }
}

void yyerror(char *s){
    return;
}
int yywrap(){
    return 1;
}

int main(){
    printf("Enter the expression: ");
    yyparse();
    threeAdd();
    printf("\n");
    //fouradd();
    printf("\n");
    //triple();
    return 0;
}