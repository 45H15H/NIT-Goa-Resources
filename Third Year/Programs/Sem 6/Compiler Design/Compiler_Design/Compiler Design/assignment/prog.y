%{
    #include<stdio.h>
    int yylex(void);
    void yyerror(char *);
    int sym[26];
%}


%token ID
%token NUM

%%
start: statement ';' {printf("Instruction is correct and Parsed successfully\n");}
     | statement ';' start 
     ;
statement: ID '=' expr {sym[$1] = $3; printf("Value is %d\n",sym[$1]);}
        | expr {printf("Value is %d\n",$1);}
        ;

expr:term '+' term {$$ = $1 + $3;}
    | term {$$ = $1;}
    ;
term: ID {$$ = sym[$1];}
    | NUM
    ;

%%

void yyerror(char *s){
    printf("%s Invalid Expression\n", s);
}
int main(void){
    printf("Input: ");
    yyparse();
    return 0;
}