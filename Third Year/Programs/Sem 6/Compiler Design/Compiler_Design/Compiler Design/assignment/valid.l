%{
    #include <stdlib.h>
    void yyerror(char *);
    #include "y.tab.h"    
%}

%%

[a-zA-Z] {
    yylval = *yytext - 'a';
    return ID;
}


[0-9]+ {
    yylval = atoi(yytext);
    return NUM;
}
[0-9]+[.][0-9]+ {
    yylval = atoi(yytext);
    return FLOAT
} 

[-+*/();=] {return *yytext;}

[ \t\n] {;}

. {yyerror("invalid charachter");}
%%

int yywrap(void) {return 1;}