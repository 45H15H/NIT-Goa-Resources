%{
    #include<stdio.h>
    int yylex(void);
    void yyerror(char *);
    int sym[26];
%}


%token ID
%token NUM IF ELSE

%%
start: statement ';' {printf("Parsed successfully\n");}
     | statement ';' start {printf("Good, Parsed successfully\n");};
     ;
statement: IDENTIFIER '=' expr {sym[$1] = $3; printf("Value is %d\n",sym[$1]);}
        | IF '(' expr ')' { }
        ;

expr:term '+' term {$$ = $1 + $3;}
    | term {$$ = $1;}
    ;
term: IDENTIFIER {$$ = sym[$1];}
    | NUMBER 
    ;

%%

void yyerror(char *s){
    printf("%s Invalid Expression\n", s);
}
int main(void){
    printf("Input: ");
    yyparse();
    return 0;
}