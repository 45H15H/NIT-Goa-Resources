%{
#include "y.tab.h"
%}

%%
"+"             { return '+'; }
"-"             { return '-'; }
"*"             { return '*'; }
"/"             { return '/'; }
"("             { return '('; }
")"             { return ')'; }
[0-9]+          { yylval = atoi(yytext); return DIGIT; }
[ \t\n]         ; // ignore whitespace and newlines
.               { return yytext[0]; }
%%

int yywrap() {
    return 1;
}


