%{
    #include<stdio.h>
    int yylex(void);
    void yyerror(char *);
    int sym[26];
%}


%token EQUALS
%token IDENTIFIER
%token NUMBER

%%
start: statement
     |start statement;
     ;
statement: IDENTIFIER EQUALS expr {sym[$1] = $3; printf("%d",sym[$1]);}
        ;

expr:IDENTIFIER '+' IDENTIFIER {$$ = sym[$1]+sym[$3];}
    |NUMBER {$$ = $1;}
    ;

%%

void yyerror(char *s){
    printf("%s Invalid Expression\n", s);
}
int main(void){
    yyparse();
    return 0;
}