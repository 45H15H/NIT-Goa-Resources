%{
    #include<stdlib.h>
    void yyerror(char *);
    #include "y.tab.h"
%}

%%
if {return IF;}
else {return ELSE;}
then {return THEN;}
[a-z] {yylval = *yytext - 'a'; return VARIABLE;}
[0-9]+ {yylval = atoi(yytext); return INTEGER;}

[-+()=/*<>\n] {return *yytext;}

[ \t] ;

. yyerror("invalid charachter\n");
%%

int yywrap(void) {return 1;}