%{
    #include <stdlib.h>
    void yyerror(char *);
    #include "y.tab.h"    
%}

%%

[a-zA-Z] {
    yylval = *yytext - 'a';
    return IDENTIFIER;
}

[=] {return EQUALS;}

[0-9]+ {
    yylval = atoi(yytext);
    return NUMBER;
}

[-+*/()] {return *yytext;}

[ \t\n] {;}

. {yyerror("invalid charachter");}
%%

int yywrap(void) {return 1;}