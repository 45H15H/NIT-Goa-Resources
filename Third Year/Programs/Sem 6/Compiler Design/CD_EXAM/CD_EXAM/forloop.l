%{
    #include <stdlib.h>
    void yyerror(char *);
    #include "y.tab.h"    
%}

%%

[a-zA-Z] {
    yylval.integer = *yytext - 'a';
    return IDENTIFIER;
}

[=] {return EQUALS;}

[0-9]+[.][0-9]* {
    yylval.floatVal = atof(yytext);
    return NUMBER;
}

[-+*/()] {return *yytext;}

[ \t\n] {;}

. {yyerror("invalid charachter");}
%%

int yywrap(void) {return 1;}