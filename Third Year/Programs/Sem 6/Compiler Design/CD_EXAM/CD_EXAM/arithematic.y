%{
    #include<stdio.h>
    int yylex(void);
    void yyerror(char *);
%}

%token NUMBER

%%
line:E '\n' {
    printf("%d\n",$1);
    printf("Valid Expression\n");return 0;}
    |'\n'
    ;
E:E '+' T {$$ = $1+$3;} 
|E '-' T {$$ = $1-$3; }
|T {$$ = $1; }
;
T:T '*' F {$$ = $1* $3; }
|T '/' F {$$ = $1/$3; }
|F { $$ = $1;}
;
F:'(' E ')' {$$ = $2; }
|NUMBER {$$ = $1; }
;
%%
void yyerror(char *s){
    printf("Invalid Expression\n");
}
int main(void){
    yyparse();
    return 0;
}