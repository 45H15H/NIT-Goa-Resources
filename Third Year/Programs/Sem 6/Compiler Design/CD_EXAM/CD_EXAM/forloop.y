%{
    #include<stdio.h>
    int yylex(void);
    void yyerror(char *);
    float sym[26];
%}

%union
{
    int integer;
    char *character;
    float floatVal;   
}

%token <character> EQUALS
%token <integer> IDENTIFIER
%token <floatVal> NUMBER

%type <floatVal> expr

%%
start: statement
     |start statement;
     ;
statement: IDENTIFIER EQUALS expr {sym[$1] = $3;printf("%f",$3);}
        ;

expr:IDENTIFIER '+' IDENTIFIER {$$ = sym[$1]+sym[$3];}
    |NUMBER {$$ = $1;}
    ;

%%

void yyerror(char *s){
    printf("%s Invalid Expression\n", s);
}

int main(void){
    yyparse();
    return 0;
}   