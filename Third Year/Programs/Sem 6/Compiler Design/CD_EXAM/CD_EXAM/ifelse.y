%{
    #include <stdio.h>
    void yyerror(char *);
    int yylex(void);
    int sym[26];    
%}
%token INTEGER VARIABLE IF ELSE THEN
%left '+' '-'
%left '*' '/'
%%

program: program statement '\n'
        |
        ;
statement : E { printf("%d\n",$1);}
        | VARIABLE '=' E {sym[$1] = $3; $$ = $3;}
        | IF E2 THEN statement ELSE statement {$$ = ($2)?$4:$6; printf("%d\n",$$);}
        ;
E: INTEGER
  | VARIABLE {$$ = sym[$1];}
  |E '+' E {$$ = $1+$3;}
  |E '-' E {$$ = $1-$3;}
  |E '*' E {$$ = $1*$3;}
  |E '/' E {$$ = $1/$3;}
  |'(' E ')' {$$ = $2;}

E2: E '>' E {$$ = ($1>$3)?1:0;}
    |E '<' E {$$ = ($1<$3)?1:0;}
    ;
%%
void yyerror(char *s){
    printf("%s Invalid Expression lex\n", s);
}
int main(void){
    yyparse();
    return 0;
}    
