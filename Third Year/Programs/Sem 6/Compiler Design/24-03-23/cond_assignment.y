%{
#include<stdio.h>
%}

//definition section

%token NUM ID IF LE GE EQ NE OR AND ELSE
%right '='
%left AND OR
%left '<' '>' LE GE EQ NE
%left '+' '-'
%left '*' '/'
%left '!'
%right UMINUS
%%

//Grammer declaration

S:		ST { printf("Input accepted.\n"); return 0; }

ST:		SMT
		| ST SMT
		;

SMT:		ASSIGN_SMT
		| IF_ELSE_SMT
		;

ASSIGN_SMT :	ID '=' E';'
		;		

IF_ELSE_SMT:	IF'('E2')' '{' ST '}' ELSE '{' ST '}'
			| IF'('E2')' '{' ST '}'

E:		ID'='E
		| E'+'E
		| E'-'E
		| E'*'E
		| E'/'E
		| E'<'E
		| E'>'E
		| E LE E
		| E GE E
		| E EQ E
		| E NE E
		| E OR E
		| E AND E
		| ID
		| NUM
		;

E2:		E'<'E
		| E'>'E
		| E LE E
		| E GE E
		| E EQ E
		| E NE E
		| E OR E
		| E AND E
		| ID
		| NUM
		;
%%


main()
{
printf("Enter the expression\n");
yyparse();
printf("\nExpression is valid\n");
exit(0);
}

//if error occured  (invalid expression)

int yyerror(char *s)
{
printf("\nExpression is invalid\n");
exit(0);
}

