%{
	#include<stdio.h>
	int vowelCount=0;
	int consonentCount =0;
%}
%%
\s {} 
[aeiouAEIOU] {vowelCount++;}
[^aeiouAEIOU\s\n] {consonentCount++;}
%%
int yywrap(){}
int main(void){
yylex();
printf("Number of vowels: %d\n",vowelCount);
printf("Number of consonents: %d",consonentCount);
return 0;
}
