%{
    #include<stdio.h>
    void yyerror(char *);
    int yylex(void);
    int count = 0,count2=0;
    void prin(int n);
%}

%token ID INTEGER IF ELSE THEN

%%

P : P X '\n'
    |
    ;
X: IF E '>' E THEN { printf("if ");prin($2);printf(">");prin($4);
  printf("goto L%d else goto L%d \n",count2,count2+1);
  } S ELSE S
  ;
  | 
  ;
S  : ID{printf("L%d : ",count2);} '=' E { printf("%c = ",'a'+$1);
                prin($4);
                printf("\n");
                printf("exit\n");
                count2++;
                }
    ;
E : E '+' T { printf("t%d = ",count);
            prin($1);
            printf("+ ");
            prin($3);
            printf("\n");
            $$ = count++;
            }
    | T { $$ = $1;}
    ;
T : T '*' F { printf("t%d = ",count);
            prin($1);
            printf("* ");
            prin($3);
            printf("\n");
            $$ = count++;
            }
    | F   { $$ = $1; }
    ;
F : ID { $$ = 'a' + $1; }

%%

void yyerror(char *s) {
    fprintf(stderr, "%s\n", s);
    return ;
}

void prin(int n){
    if( n < 'a' ){
        printf("t%d ",n);
    }
    else{
        printf("%c ",n);
    }
}

int main()
{
	yyparse();
	return 0;
}