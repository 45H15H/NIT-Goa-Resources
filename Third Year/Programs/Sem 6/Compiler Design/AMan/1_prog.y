%{
#include <stdio.h>
#include <string.h>
#include "y.tab.h"
extern char* yytext;
%}

//definition section

%token ID NUM IF ELSE GE OS CS OB CB comma
%right '='
%left GE
%left OS CS
%left '+' '-'
%left '*' '/'
%right MINUS
%%

program: program line '\n'
	|
	;


line: IF{if_fun();} '(' E1 ')' {rem();} OB E comma CB ELSE {else_fun()} OB Expr comma CB {End();}
;
E1:		E GE E{func();}
;

E:		V'='{push();} E{codegen_assign();}
		| E'+'{push();} E{codegen();}
		| E'-'{push();} E{codegen();}
		| E'*'{push();} E{codegen();}
		| E'/'{push();} E{codegen();}
		| '(' E ')'
		| '-'{push();} E{codegen_umin();} %prec MINUS
		| V
		| NUM{push();} 
		;
Expr :    	V'='{push();} Expr{codegen_assign();}
		|Expr '+' {push();} Expr{codegen();}
		| V F
		;

F: 		OS {push();} V CS {push();} {codearray();} F
		|
		;
		


V:		ID {push();} 
		;
%%

char st[100][10];
int top=0;
char temp[3]="t0";

int main()
{
printf("Enter the expression\n");
yyparse();
}

int push()
{
strcpy(st[++top],yytext);
}

int codearray()
{
printf("%s = %s %s %s %s\n",temp,st[top-3],st[top-2],st[top-1],st[top]);
top-=3;
strcpy(st[top],temp);
temp[1]++;
}

int codegen()
{
printf("%s = %s %s %s\n",temp,st[top-2],st[top-1],st[top]);
top-=2;
strcpy(st[top],temp);
temp[1]++;
}

int codegen_umin()
{
printf("%s = %s %s\n",temp,st[top-1],st[top]);
top--;
strcpy(st[top],temp);
temp[1]++;
}

int codegen_assign()
{
printf("%s = %s\n",st[top-2],st[top]);
top-=2;
}

int func(){
printf("%s < %s ",st[top-1],st[top]); 
top--;
}

int if_fun()
{
printf("\nL1: \n");
printf("IF ");
}

int rem()
{
printf("then goto L2\n");
}

int else_fun()
{
printf("goto END\n");
printf("L2:\n");
}


int End()
{
printf("End\n\n");
}



int yyerror(char *s)
{
printf("\nExpression is invalid\n");
exit(0);
}