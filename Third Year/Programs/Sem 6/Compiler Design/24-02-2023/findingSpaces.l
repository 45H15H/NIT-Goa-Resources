%{
    #include<stdio.h>
    int spaces = 0;
    int tabs = 0;
    int newlineCount=0;
%}
tab[\t]
space[' ']
newline[\n]
%%
{tab} {tabs++;}
{space} { spaces++;}
{newline} {newlineCount++;}
.  ;
%%

int yywrap(){
    printf("I am done");
}

int main(void){
    FILE *fileInput = fopen("newdata.txt","r");
    yyin = fileInput;
    yylex();
    printf("Number of spaces: %d\n",spaces);
    printf("Number of tabs: %d\n",tabs);
    printf("Number of lines: %d",newlineCount);
    fclose(fileInput);
}
