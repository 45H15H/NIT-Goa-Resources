%{
    // #include<stdio.h>
    int keyword_count = 0;
%}

%%

"if "|else|while|int|switch|for|char    {
    keyword_count++;
    printf("This is a keyword");
}
. {
    printf("Not a keyword");
}
%%

int yywrap(){
}

int main(void){
    yylex();
    printf("Number of keywords: %d",keyword_count);
}