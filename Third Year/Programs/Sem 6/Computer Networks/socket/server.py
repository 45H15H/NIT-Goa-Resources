import socket
# import sys

sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

server_address = ("localhost", 10000)

print(f"Starting port on {server_address[0]} and port {server_address[1]}")
sock.bind(server_address)

sock.listen(1)

while True:
    print("Waiting for a connnection")

    connection, clientAddress = sock.accept()

    try:
        print(f'Connection from {clientAddress}')
        while True:
            data = connection.recv(160)
            print(f'received data: {data}')
            if data:
                data = data.upper()
                print("Sending data to client")
                connection.sendall(data)
            else: 
                print(f"No more data from {clientAddress}")
                break
    finally:
        connection.close()
        



