import socket

sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

serverAddress = ('localhost',8000)

sock.bind(serverAddress)
sock.listen(1)
print(f'Server has established at {serverAddress[0]}')

while True:
    connection, clientAddress = sock.accept()
    print(f'Accepting connection from {clientAddress}')
    try:
        fileRequested = connection.recv(1024)
        file = open(str(fileRequested.decode()),"r")
        print("File name received")
        
        line = file.read(1024)
        while(line):
            connection.send(line.encode())
            line = file.read(1024)
        connection.close()
        break
    finally:
        connection.close()
    
