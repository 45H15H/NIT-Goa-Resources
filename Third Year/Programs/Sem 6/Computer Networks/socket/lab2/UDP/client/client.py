import socket

sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

clientAddress = ('localhost',8000)

try:
    filename = input("Enter the name of the file: ")
    print(f"Connection established to {clientAddress[0]}")
    sock.sendto(filename.encode(),clientAddress)

    file = open(str(filename),"w")
    print("Receiving data")
    packet = sock.recvfrom(1024)
    data = packet[0].decode()
    for line in data.split('\n'):
        print(line)
        file.write(str(line))
        file.write('\n')
    print("File has opened")
finally:
    pass
sock.close()
print("Successfully got the file")
    