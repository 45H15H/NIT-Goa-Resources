import socket 

sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

server_address = ('localhost',10000)

print(f'Connecting to {server_address[0]} on port {server_address[1]}')

sock.connect(server_address)

try:
    message = b'This is a simple text we want to capitalize.'
    # message = message.encode('utf-8')
    print(f'Sending {message}')

    sock.sendall(message)

    amount_received = 0
    amount_expected =len(message)

    while amount_received < amount_expected:
        data = sock.recv(160)
        amount_received += len(data)
        data = data.decode()
        print(f'Received {data}')
finally:
    print("Closing socket")
    sock.close()