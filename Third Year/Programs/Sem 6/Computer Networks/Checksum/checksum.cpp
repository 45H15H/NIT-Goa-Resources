#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<b;i++)
#define rrep(i,a,b) for(int i=a;i>=b;i--)
#define ll long long
#define ull unsigned ll
#define take(n) int n;cin>>n
#define mod 1000000007
#define pii pair<int,int>
#define v(i) vector<i>
#define mp(a,b) make_pair(a,b)
#define pb(a) push_back(a)
#define pp pop_back()
#define array(n,name) ll int *name=new ll int[n]
#define takearray(n,name) rep(i,0,n) cin>>name[i];
#define printarray(n,nums) rep(i,0,n) cout<<nums[i]<<" ";
#define Zubin ios::sync_with_stdio(false);
#define Shah cin.tie(NULL);cout.tie(0);
#define nl cout<<endl;
using namespace std;

int xcor[4]={-1,0,0,+1};
int ycor[4]={0,-1,1,0};

ull int power(ull n,int x){
    if(x==0) return 1;
    return n * power(n,x-1);
}

string partialSum(string frame1,string frame2,int n){
    int carry = 0;
    int sum = 0;
    string output = "";
    for(int i=n-1;i>=0;i--){
        sum = carry + frame1[i]-'0' + frame2[i]-'0';
        char current = sum%2 + '0';
        output = current + output;
        carry = sum/2;
    }
    for(int i=n-1;carry && i>=0;i--){
        if(carry==0) break;
        int sum = carry + output[i]-'0';
        char current  = sum%2 + '0';
        output[i] = current;
        carry = sum/2;
    }
    if(carry) output[n-1] = '1';
    return output;
    
}


string findChecksum(string data,int blockSize){
    int n = data.length();
    int numBlocks = n/blockSize;
    int carry = 0;
    string finalSum = "";
    string frame1 = data.substr(0,blockSize);
    int i = blockSize;
    while(i<n){
        string frame2 = data.substr(i,blockSize);
        frame1 = partialSum(frame1,frame2,blockSize);
        i+=blockSize;
    }
    for(int i=0;i<blockSize;i++){
        frame1[i] = '0' + (1-(frame1[i]-'0'));
    }
    return frame1;
}

bool validateChecksum(string encodedData,int blocksize){
    string checksum = findChecksum(encodedData,blocksize);
    if(stoi(checksum)==0) return true;
    else return false;
}

string decodeData(string encodedData,int blocksize){
    int dataLength = encodedData.size() - blocksize;
    return encodedData.substr(0,dataLength);
}

int main(){

    srand(time(0));


    //Sender 


    int blockSize;
    string data;
    cout<<"Enter the data: ";
    cin>>data;
    cout<<"Enter the blocksize: ";
    cin>>blockSize;
    cout<<"Data:              "<<data<<endl;
    string checksum = findChecksum(data,blockSize);
    cout<<"Checksum: "<<checksum<<endl;
    string encodedData = data + checksum;
    cout<<"Encoded data sent: "<<encodedData;
    nl;


    // Sender




    // Noisy Channel

    double probCorruption = (rand()%101)/100.0;
    if(probCorruption>0.5){
        int randIndex = rand()%encodedData.size();
        encodedData[randIndex] = '0'+(1-(encodedData[randIndex]-'0'));

        cout<<"Corrupted data:    "<<encodedData<<endl;
    }

    cout<<probCorruption<<endl;

    // Noisy Channel 





    // Receiver


    if(validateChecksum(encodedData,blockSize)){
        cout<<"No error detected"<<endl;
        data = decodeData(encodedData,blockSize);
        cout<<"Decoded data:     "<<data<<endl;
    }
    else{
        cout<<"Error detected"<<endl;
    }


    // Receiver


return 0;
}