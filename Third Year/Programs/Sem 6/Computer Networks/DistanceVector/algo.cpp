#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<b;i++)
#define rrep(i,a,b) for(int i=a;i>=b;i--)
#define ll long long
#define ull unsigned ll
#define take(n) int n;cin>>n
#define mod 1000000007
#define mp(a,b) make_pair(a,b)
#define pb(a) push_back(a)
#define pp pop_back()
#define array(n,name) ll int *name=new ll int[n]
#define takearray(n,name) rep(i,0,n) cin>>name[i];
#define printarray(n,nums) rep(i,0,n) cout<<nums[i]<<" ";
#define Zubin ios::sync_with_stdio(false);
#define Shah cin.tie(NULL);cout.tie(0);
#define nl cout<<endl;
using namespace std;


class Server{
    public:
    char serverName;
    vector<int> cost;
    vector<bool> neighbours;
    vector<int> nextHop;
    Server(char name){
        serverName = name;
    }
    void printServer(){
        cout<<"> Routing table of node: "<<serverName<<endl;
        cout<<"Destination"<<" "<<"  Cost  "<<" "<<"Next Hop"<<endl;
        rep(i,0,cost.size()){
            if(serverName != 'A'+i){
                char next;
                if(nextHop[i]) next = 'A'+nextHop[i];
                else next = '-';
                string currentCost = cost[i]==INT_MAX ? "Inf":to_string(cost[i]);

                cout<<setw(6)<<char('A'+i)<<" "<<setw(10)<<currentCost<<" "<<setw(string("Next Hop").size())<<next<<endl;
            }
        }
    }
};

class Network{
    int numberOfServers;
    vector<Server*> servers;
    vector<vector<int>> networkLinks;
    public:
    Network(int n,vector<vector<int>> &linkRecord){
        numberOfServers = n;
        rep(i,0,n){
            Server *newServer=new Server('A'+i);
            rep(j,0,n){
                newServer->cost.pb(linkRecord[i][j]);
                newServer->nextHop.pb(linkRecord[i][j]==INT_MAX ? 0:j);
                newServer->neighbours.pb(linkRecord[i][j]!=INT_MAX);
            }
            servers.pb(newServer);
        }
        networkLinks = linkRecord;
    }
    void completeNextIteration(){
        //storing new network links in a separate vector
        vector<vector<int>> storeLinkCosts(numberOfServers);
        rep(i,0,numberOfServers){
            rep(j,0,numberOfServers){
                if(i==j) storeLinkCosts[i].push_back(0);
                else storeLinkCosts[i].push_back(servers[i]->cost[j]);
            }
        }
        rep(i,0,servers.size()){
            changeAndTriggerServer(i,storeLinkCosts);
        }
        rep(i,0,numberOfServers){
            rep(j,0,numberOfServers){
                if(i!=j) servers[i]->cost[j] = storeLinkCosts[i][j];
            }
        }
    }

    void printNetwork(char serverName='*'){
        if(serverName!='*'){
            servers[serverName-'A']->printServer();
        }
        else{
            rep(i,0,servers.size()){
                servers[i]->printServer();
                nl;
            }
        }
    }
    void changeAndTriggerServer(int serverNumber,vector<vector<int>> &storeLinkCost){
        int n = servers.size();
        Server *current = servers[serverNumber];
        rep(i,0,n){
            if(serverNumber!=i && current->neighbours[i]){
                rep(j,0,n){
                    if(serverNumber!=j && j!=i &&  servers[i]->cost[j]!=INT_MAX && (networkLinks[serverNumber][i] + servers[i]->cost[j] < current->cost[j])){
                        storeLinkCost[serverNumber][j] = networkLinks[serverNumber][i] + servers[i]->cost[j];
                        current->nextHop[j] = i;
                    }
                }
            }
        }
    }
};



int main(){


    cout<<"Enter the number of servers: ";
    take(n);
    cout<<"Enter the number of links: ";
    take(m);
    cout<<"Enter the link src and dest one by one\n";
    vector<vector<int>> linkRecord(n,vector<int>(n,INT_MAX));
    rep(i,0,m){
        char server1, server2;
        int linkCost;
        cin>>server1>>server2>>linkCost;
        linkRecord[server1-'A'][server2-'A'] = linkCost;
        linkRecord[server2-'A'][server1-'A'] = linkCost;
    }
    Network net(n,linkRecord);
    cout<<"\n\n>>  Initial Routing Table\n\n";
    net.printNetwork();
    net.completeNextIteration();
    cout<<"\n\n>>   Routing Table after one iteration of the algorithm\n\n";
    net.printNetwork();
    net.completeNextIteration();
    cout<<"\n\n>>   Routing Table after two iteration of the algorithm\n\n";
    net.printNetwork();
return 0;
}