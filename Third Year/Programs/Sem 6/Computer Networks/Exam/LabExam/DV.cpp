#include<bits/stdc++.h>
using namespace std;


#define INT_MAXI 60000
#define CHANGE 1


class Server{
    public:
    char serverName;
    vector<int> costs;
    vector<bool> neighbours;
    vector<char> nextHop;
    Server(char name){
        serverName = name;
    } 
    void printServer(){
        cout<<"Routing table at: "<<serverName<<endl;
        cout<<"Destination    Cost     NextHop"<<endl;
        int n = neighbours.size();
        for(int i=0;i<n;i++){
            if(i!='Z'-serverName){
                string currCost = costs[i]>=INT_MAXI ? "Inf": to_string(costs[i]);
                cout<<setw(6)<<char('Z'-i)<<setw(12)<<currCost<<setw(10)<<nextHop[i]<<endl;
            }
        }
        cout<<endl;
    }
};

class Network{
    public:
    int numServers;
    vector<Server*> servers;
    vector<vector<int>> networkLinks;
    Network(int n,vector<vector<int>> &cost){
        numServers = n;
        for(int i=0;i<n;i++){
            Server *newServer = new Server('Z'-i);
            for(int j=0;j<n;j++){
                newServer->costs.push_back(j==i ? 0:cost[i][j]);
                newServer->neighbours.push_back(cost[i][j]!=INT_MAXI);
                newServer->nextHop.push_back(cost[i][j]!=INT_MAXI ? 'Z'-j:'-');
            }
            servers.push_back(newServer);
        }
        networkLinks = cost;
        for(int i=0;i<n;i++){
            networkLinks[i][i] = 0;
        }
    }
    void completeAllIterations(){
        int it = 1;
        while(completeNextIteration()==CHANGE){
            continue;
        }
        printNetwork();
        cout<<"All Iterations have been completed\n\n\n";
    }
    int completeNextIteration(){
        int n = numServers;
        vector<vector<int>> currentCosts(numServers);
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){ 
                currentCosts[i].push_back(servers[i]->costs[j]);
            }
        }
        for(int i=0;i<n;i++){
            computeServer(i,currentCosts);
        }
        int changeFlag = false;
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(servers[i]->costs[j]!=currentCosts[i][j]){
                    changeFlag =true;
                }
                servers[i]->costs[j] = currentCosts[i][j];
            }
        }
        return changeFlag;
    }
    void computeServer(int in,vector<vector<int>> &currentCosts){
        Server *current = servers[in];
        int n = numServers;
        for(int i=0;i<n;i++){
            int destCost = INT_MAXI;
            if(i==in) continue;
            for(int j=0;j<n;j++){
                if(current->neighbours[j]){
                    destCost  = min(destCost,networkLinks[in][j] + servers[j]->costs[i]);
                }
            }
            currentCosts[in][i] = destCost;
            for(int j=0;j<n;j++){
                if(current->neighbours[j]){
                    if(destCost==networkLinks[in][j] + servers[j]->costs[i]){
                        if(current->nextHop[i]!=j)
                            current->nextHop[i] = 'Z'-j;
                        break;
                    }
                }
            }
        }
    }

    void printNetwork(){
        for(int i=0;i<numServers;i++){
            servers[i]->printServer();
        }
    }
};


int main(){
    int n;
    cin>>n;
    int m;
    cin>>m;
    vector<vector<int>> linkCost(n,vector<int>(n,INT_MAXI));
    for(int i=0;i<m;i++){
        char link1,link2;
        int cost;
        cin>>link1>>link2>>cost;
        linkCost['Z'-link1]['Z'-link2] = cost;
        linkCost['Z'-link2]['Z'-link1] = cost;
    }
    Network net(n,linkCost);
    net.printNetwork();
    cout<<"\n\n\n>>>>Start of Distance Vector Algorithm\n\n";
    net.completeAllIterations();
    net.networkLinks[1][6] = INT_MAXI;
    net.networkLinks[6][1] = INT_MAXI;
    cout<<"There is a link failure in y-t link\n\n\n";
    net.completeAllIterations();
    return 0;

}