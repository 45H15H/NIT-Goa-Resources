char *caeserCipherEncrypt(char *str, int key){
    int i = 0;
    while(str[i] != '\0'){
        if(str[i] >= 'a' && str[i] <= 'z'){
            str[i] = (str[i] - 'a' + key) % 26 + 'a';
        }
        else if(str[i] >= 'A' && str[i] <= 'Z'){
            str[i] = (str[i] - 'A' + key) % 26 + 'A';
        }
        i++;
    }
    return str;
}

char *caeserCipherdecrypt(char *str,int key){
    int i = 0;
    while(str[i] != '\0'){
        if(str[i] >= 'a' && str[i] <= 'z'){
            str[i] = (str[i] - 'a' - key + 26) % 26 + 'a';
        }
        else if(str[i] >= 'A' && str[i] <= 'Z'){
            str[i] = (str[i] - 'A' - key + 26) % 26 + 'A';
        }
        i++;
    }
    return str;
}

char* prepareKeyTable(char *key){

    #define PRESENT_IN_KEY 1
    #define NOT_IN_KEY 0
    #define KEY_VISITED 2
    #define SKIP -1
    char keyTable[5][5];
    int dict[26] = {0};
    for(int i=0;key[i]!='\0';i++){
        if(key[i]!='j'){
            dict[key[i]-'a'] = PRESENT_IN_KEY;
        }
    }
    dict['j'-'a'] = SKIP;
    int i = 0;
    int j = 0;
    for(int k=0;key[k]!='\0';k++){
        if(dict[key[k]-'a'] == PRESENT_IN_KEY){
            keyTable[i][j] = key[k];
            dict[key[k]-'a'] = KEY_VISITED;
            j++;
            if(j==5){
                i++;
                j=0;
            }
        }
    }
    // for(int i=0;i<26;i++){
    //     printf("%d ",dict[i]);
    // }
    for(int k=0;k<26;k++){
        if(dict[k]== NOT_IN_KEY){
            keyTable[i][j] = k+'a';
            j++;
            if(j==5){
                i++;
                j=0;
            }
        }
    }
    // for(int i=0;i<5;i++){
    //     for(int j=0;j<5;j++){
    //         printf("%c ",keyTable[i][j]);
    //     }
    //     printf("\n");
    // }
    


    return keyTable;
}
char *playfairCipherEncrypt(char *str,char *key){

    char *keyTable = prepareKeyTable(key);
    return str;
    // for(int i=0;key[i]!='\0';i+=2){

    // }
}