import time

def delayed_print(text, delay=0.05):
    for char in text:
        print(char, end='', flush=True)  # end='' keeps printing on the same line, flush=True forces immediate printing
        time.sleep(delay)

output = "This is a delayed output!"
delayed_print(output)
