import column_transposition
import route_cipher
import row_column_transposition
import rail_fence_cipher

def main():
    print("Choose a cipher method:")
    print("1. Column Transposition")
    print("2. Route Cipher")
    print("3. Row Column Transposition")
    print("4. Rail Fence Cipher")
    
    choice = input("Enter your choice (1-4): ")
    
    if choice == '1':
        text = input("Enter the text to encrypt: ")
        key = input("Enter the key: ")
        encrypted_text = column_transposition.encrypt(text, key)
        print(f"Encrypted Text: {encrypted_text}")
        decrypted_text = column_transposition.decrypt(encrypted_text, key)
        print(f"Decrypted Text: {decrypted_text}")
        
    elif choice == '2':
        text = input("Enter the text to encrypt: ")
        key = input("Enter the key: ")
        encrypted_text = route_cipher.encrypt(text, key)
        print(f"Encrypted Text: {encrypted_text}")
        decrypted_text = route_cipher.decrypt(encrypted_text, key)
        print(f"Decrypted Text: {decrypted_text}")
        
    elif choice == '3':
        text = input("Enter the text to encrypt: ")
        key = input("Enter the key: ")
        encrypted_text = row_column_transposition.encrypt(text, key)
        print(f"Encrypted Text: {encrypted_text}")
        decrypted_text = row_column_transposition.decrypt(encrypted_text, key)
        print(f"Decrypted Text: {decrypted_text}")
        
    elif choice == '4':
        text = input("Enter the text to encrypt: ")
        key = int(input("Enter the key (number of rails): "))
        encrypted_text = rail_fence_cipher.encrypt(text, key)
        print(f"Encrypted Text: {encrypted_text}")
        decrypted_text = rail_fence_cipher.decrypt(encrypted_text, key)
        print(f"Decrypted Text: {decrypted_text}")
        
    else:
        print("Invalid choice. Please choose a valid option.")

if __name__ == "__main__":
    main()